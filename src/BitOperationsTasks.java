
public class BitOperationsTasks {
	public static void main(String[] args) {
		// 1010 1010   - number
		// -> 6
		// 0000 0001
		// << 5
		// 0010 0000   - mask
		
		//  number
		// &
		//	mask
		
		//  10?0 1010 // ? 1 or 0
		// &
		//	0010 0000
		//	---------
		//	0010 0000
		
		// 1 variant
		//  1000 1010 // ? - 0 
		// &
		//	0010 0000
		//	---------
		//	0000 0000 // = 0
		
		// 2 variant
		//  1010 1010 // ? - 1 
		// &
		//	0010 0000
		//	---------
		//	0010 0000 // != 0
		
		int number = 	0b10_1_01010;
		int number2 = 	0b10_0_01010;
		int mask = 1 << 5;
		
		// 7 + (5 * 4)
		if ((number & mask) == 0)
			System.out.println("0");
		else
			System.out.println("1");
		
		if ((number2 & mask) == 0)
			System.out.println("0");
		else
			System.out.println("1");
	}
}