import java.math.BigInteger;
import java.text.DecimalFormat;

public class Shift {
	public static void main(String[] args) {
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("0000,0000,0000,0000,0000,0000,0000,0000");
		int a = 1;
		// << Побитовый сдвиг влево
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(a))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(a << 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(a << 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(a << 3))));
		
		System.out.println(a);		// 1
		System.out.println(a << 1); // 1 * 2^1
		System.out.println(a << 2); // 1 * 2^2
		System.out.println(a << 3); // 1 * 2^3
		
		System.out.println(5 << 3); // 5 * 2^3 = 5 * 8 = 40
		
		System.out.println("----------------------------");
		// >> Побитовый сдвиг вправо
		int b = 0b1000;
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(b))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(b >> 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(b >> 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(b >> 3))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(b >> 4))));
		
		System.out.println(b);		// 8
		System.out.println(b >> 1); // 8 / 2^1
		System.out.println(b >> 2); // 8 / 2^2
		System.out.println(b >> 3); // 8 / 2^3
		System.out.println(b >> 4); // 8 / 2^4
		
		int c = 0b1111;
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(c))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(c >> 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(c >> 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(c >> 3))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(c >> 4))));
		System.out.println(c);		// 15
		System.out.println(c >> 1); // 15 / 2^1
		System.out.println(c >> 2); // 15 / 2^2
		System.out.println(c >> 3); // 15 / 2^3
		System.out.println(c >> 4); // 15 / 2^4
		
		int d = -17;
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(d))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(d >> 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(d >> 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(d >> 3))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(d >> 4))));
		System.out.println(d);		// 15
		System.out.println(d >> 1); // 15 / 2^1
		System.out.println(d >> 2); // 15 / 2^2
		System.out.println(d >> 3); // 15 / 2^3
		System.out.println(d >> 4); // 15 / 2^4
		
		System.out.println("----------------------------");
		int e = 19;
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(e))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(e >>> 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(e >>> 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(e >>> 3))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(e >>> 4))));
		System.out.println(e);		// 19
		System.out.println(e >>> 1); // 19 / 2^1
		System.out.println(e >>> 2); // 19 / 2^2
		System.out.println(e >>> 3); // 19 / 2^3
		System.out.println(e >>> 4); // 19 / 2^4
		
		int f = -19;
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(f))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(f >>> 1))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(f >>> 2))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(f >>> 3))));
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(f >>> 4))));
		System.out.println(f);		// 19
		System.out.println(f >>> 1); // 19 / 2^1
		System.out.println(f >>> 2); // 19 / 2^2
		System.out.println(f >>> 3); // 19 / 2^3
		System.out.println(f >>> 4); // 19 / 2^4
	}
}