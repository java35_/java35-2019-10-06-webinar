import java.math.BigInteger;
import java.text.DecimalFormat;

public class BitOperations {
	public static void main(String[] args) {
		// Вывод числа на коснсоль
		System.out.println(5);
		System.out.println(Integer.toBinaryString(5));
		
		// Задать число число в двоичном представлении
		int a = 0b101;
		System.out.println(a);
		System.out.println(Integer.toBinaryString(a));
		
		// ()2 -> ()10
		// 101 -> 1 * 2^0 + 0 * 2^1 + 1 * 2^2 = 1 + 0 + 4 = 5
		
		System.out.println(0b1011);
		
		// AND
		//  1010
		// &
		//	0110
		//	----
		//	0010
		
		//  1001
		// &
		//	0110
		//	----
		//	0000
		
		//  1001
		// &
		//	1110
		//	----
		//	1000
		
		//  1001
		// &
		//	1000
		//	----
		//	1000
		
		// OR
		//  1010
		// |
		//	0110
		//	----
		//	1110
		
		//  1001
		// |
		//	0110
		//	----
		//	1111
		
		//  1001
		// |
		//	1110
		//	----
		//	1111
		
		//  1001
		// |
		//	1000
		//	----
		//	1001
		
		// XOR
		//  1010
		// ^
		//	0110
		//	----
		//	1100
		
		//  1001
		// ^
		//	0110
		//	----
		//	1111
		
		//  1001
		// ^
		//	1110
		//	----
		//	0111
		
		//  1001
		// ^
		//	1000
		//	----
		//	0001
		
		System.out.println("***********");
		System.out.println(Integer.toBinaryString(0b0011 ^ 0b1000));
		
		// NOT
		// 1010
		// ~
		// ----
		// 0101
		System.out.println("***********");
		int b = 0b1010;
		// 0000 0000 0000 0000 0000 0000 0000 1010
		// ~
		// ---------------------------------------
		// 1111 1111 1111 1111 1111 1111 1111 0101
		System.out.println(Integer.toBinaryString(~b));
		
		// 0000 0000 0000 0000 0000 0000 0000 0000
		
		System.out.println("***********************************");
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("0000,0000,0000,0000,0000,0000,0000,0000");
		System.out.printf("%s\n", df.format(new BigInteger(Integer.toBinaryString(0b1010))));
		
		
		System.out.println("-----------------------------------");
		int number0 = 0;
		System.out.printf("%s\n", df.format(new BigInteger(Integer.toBinaryString(number0))));
		
		// ~number0
		// Integer.MIN_VALUE | Integer.MAX_VALUE
		// -1
		int number1 = -1;
		System.out.printf("%s\n", df.format(new BigInteger(Integer.toBinaryString(number1))));
		
		
		System.out.println("+++++++++++++++++++++++++++++++++");
		// X & number0 -> 0
		//  1010
		// &
		//	0000
		//	----
		//	0000
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 & number0))));
		
		// X & number1 -> x
		//  1010
		// &
		//	1111
		//	----
		//	1010
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 & number1))));
		
		// X & X -> x
		//  1010
		// &
		//	1010
		//	----
		//	1010
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 & 0b1010))));
		
		System.out.println("+++++++++++++++++++++++++++++++++");
		// X | number0 -> x
		//  1010
		// |
		//	0000
		//	----
		//	1010
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 | number0))));
		
		// X | number1 -> number1
		//  1010
		// |
		//	1111
		//	----
		//	1111
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 | number1))));
		
		// X | X -> X
		//  1010
		// |
		//	1010
		//	----
		//	1010
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 | 0b1010))));
		
		System.out.println("+++++++++++++++++++++++++++++++++");
		// X ^ number0 -> X
		//  1010
		// ^
		//	0000
		//	----
		//	1010
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 ^ number0))));
		
		// X ^ number1 -> ~X
		//  1010
		// ^
		//	1111
		//	----
		//	0101
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 ^ number1))));
		
		// X ^ X -> number0
		//  1010
		// ^
		//	1010
		//	----
		//	0000
		System.out.printf("%s\n", df.format(
				new BigInteger(Integer.toBinaryString(0b1010 ^ 0b1010))));
		
		
		
	}
}